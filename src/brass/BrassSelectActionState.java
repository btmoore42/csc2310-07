package brass;

class BrassSelectActionState implements BrassState
{
	private BrassGame brass_game;
	
	public BrassSelectActionState(BrassGame game)
	{
		brass_game = game;
	}
	
	public void mouseClicked(int x_click, int y_click)
	{
		//DO THIS
		//was a valid action clicked on?
		int brass_action_id = brass_game.getSelectedAction(x_click,y_click);
		
		//loan action (any of the three loan amounts)
		//the loan action id is 1 for 10, 2 for 20, 3 for 30
		//refer to BrassEnum to figure out how to decode brass_action_id
		if (brass_action_id <= BrassActionEnum.LOAN_30.getValue())
		{
			int loan_amount = brass_action_id*10;
			//DO THIS
			//tell the game that a loan was selected and the amount
			//tell the game to advance to the next player (playerActionCompleted)
			//tell the game to change the state back to select card state
			brass_game.loanActionSelected(loan_amount);
			brass_game.playerActionCompleted();
			brass_game.changeState(brass_game.getSelectCardState());
			
			
			
			
		}
		
		//discard
		else if (brass_action_id == BrassActionEnum.DISCARD.getValue())
		{
			//DO THIS
			//tell the game to advance to the next player
			//tell the game to change the state back to select card state
			brass_game.playerActionCompleted();
			brass_game.changeState(brass_game.getSelectCardState());
			
		}
		
		//cancel card selection
		else if (brass_action_id == BrassActionEnum.CANCEL.getValue())
		{
			//DO THIS
			//tell the game to cancel the card selection (do not advance to the next player)
			//tell the game to change the state back to select card state
			brass_game.cancelCardSelection();
			brass_game.changeState(brass_game.getSelectCardState());
			
			

		}
		
		//link action
		else if (brass_action_id == BrassActionEnum.LINK.getValue())
		{
			//DO THIS
			//change to BrassLinkActionState (more for the player to decide)
			brass_game.changeState(brass_game.getLinkActionState());
			
			
		}
		
	}
}
